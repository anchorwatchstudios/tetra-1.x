<?php 

require_once "../src/Tetra.php";
require_once "../src/XMLCalendarDatabase.php";

date_default_timezone_set("UTC");
session_start();
$DATABASE_PATH = "../src/events.xml";
Tetra::loadSkins("../res/skin/*");
Tetra::loadSettings("../src/settings.xml");
$calendar = XMLCalendarDatabase::load($DATABASE_PATH);
$skinPath = Tetra::$SETTINGS->getSkin()->getPath();
$status = "";
$id = DateTimeEvent::getCount();
$name = "";
$startTime = "";
$finishTime = "";
$date = "";
$html = "";

if($_SERVER["REQUEST_METHOD"] == "GET") {
	
	if(isset($_GET["id"])) {
		
		$id = $_GET["id"];
		
		if($id < DateTimeEvent::getCount()) {
		
			$e = $calendar->getEvent($id);
			$name = $e->getName();
			$startTime = date_format($e->getStartTime(), "g:ia");
			$finishTime = date_format($e->getFinishTime(), "g:ia");
			$date = $e->getDate();
			$html = $e->getHTML();
			
		}
		else if(isset($_SESSION["postData"])) {
			
			$e = $_SESSION["postData"];
			$name = $e["name"];
			$startTime = $e["startTime"];
			$finishTime = $e["finishTime"];
			$date = $e["date"];
			$html = $e["html"];
			unset($_SESSION["postData"]);
		
		}
		
	}
	
	if(isset($_GET["status"])) {
		
		$status = Tetra::getStatusById($_GET["status"]);
		
	}
	
}
else if($_SERVER["REQUEST_METHOD"] == "POST") {

	$redirectId = "";
	
	if(isset($_POST["save"])) {
		
		$valid = Tetra::validateInput($_POST);
		$status = Tetra::getStatusById($valid);
		
		if($valid == Tetra::$SAVED) {
			
			$name = $_POST["name"];
			$html = $_POST["html"];
			$startDateTime = DateTime::createFromFormat("m/d/Y g:ia", $_POST["date"] . " " . $_POST["startTime"]);
			$finishDateTime = DateTime::createFromFormat("m/d/Y g:ia", $_POST["date"] . " " . $_POST["finishTime"]);
			
			if(intval($_POST["id"]) == DateTimeEvent::getCount()) {
				//TODO: HERE FINISH TIME
				$calendar->addEvent(new DateTimeEvent($name, $startDateTime, $finishDateTime, $html));
				$redirectId = "?id=" . (DateTimeEvent::getCount() - 1) . "&status=" . $valid;
				XMLCalendarDatabase::save($calendar, $DATABASE_PATH);
			
			}
			else {
				//TODO: HERE FINISH TIME
				$calendar->removeEvent($_POST["id"]);
				$calendar->addEvent(new DateTimeEvent($name, $startDateTime, $finishDateTime, $html));
				$redirectId = "?id=" . (DateTimeEvent::getCount() - 2) . "&status=" . $valid;
				XMLCalendarDatabase::save($calendar, $DATABASE_PATH);

			}
			
		}
		else {
			
			$_SESSION["postData"] = $_POST;
			$redirectId = "?id=" . $_POST["id"] . "&status=" . $valid;
			
		}
		
	}
	else {
		
		if($_POST["id"] != DateTimeEvent::getCount()) {
			
			$calendar->removeEvent($_POST["id"]);
			
			if($calendar->getSize() != 0) {
					
				$redirectId = "?id=" . (DateTimeEvent::getCount() - 2) . "&status=" . $valid;
					
			}
			else {
			
				$redirectId = "?status=" . $valid;
			
			}
			
			XMLCalendarDatabase::save($calendar, $DATABASE_PATH);
			
		}
		
	}
	
	header("Location: " . dirname($_SERVER["PHP_SELF"]) . $redirectId);
	exit;
	
}

?>

<!DOCTYPE html>

<html>

	<head>
	
		<link rel="stylesheet" type="text/css" href="<?php echo $skinPath;?>">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="../lib/jquery-timepicker/jquery.timepicker.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="../lib/jquery-timepicker/jquery.timepicker.min.js"></script>
		<script>

			$(document).ready(function() {

				$("#dateField").datepicker();
				$("#startTimeField").timepicker();
				$("#finishTimeField").timepicker();

			});
			
		</script>
	
	</head>

	<body>
		
		<div class="Header">
		
			<div class="Logo">TetraCal</div>
			
			<a href="<?php echo dirname($_SERVER["PHP_SELF"]) . "/settings";?>" id="settingsButton" class="Button">Settings</a>
			
			<div class="TitleBar">
			
				<div class="ListViewTitle">Events</div>
				<div class="EventPanelTitle">Editor</div>
			
			</div>
		
		</div>
		
		<div class="Container">
		
			<div class="ListView">
			
				<?php 
				
				foreach($calendar->getEvents() as $event) {
					
					if($id == $event->getId()) {
						
						echo "<a href='" . dirname($_SERVER["PHP_SELF"]) . "?id=" . $event->getId() . "'><div class='ButtonSelected'>" . $event->getName() . "</div></a>";
						
					}
					else {
						
						echo "<a href='" . dirname($_SERVER["PHP_SELF"]) . "?id=" . $event->getId() . "'><div class='Button'>" . $event->getName() . "</div></a>";
						
					}
					
				}
				
				?>
			
			</div>
					
			<div class="EventPanel">
			
				<form method="GET" action="<?php echo htmlspecialchars(dirname($_SERVER["PHP_SELF"]));?>">
				
					<input type="submit" id="newButton" class="Button" value="+">
				
				</form>
				
				<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				
					<input name="id" type="hidden" value="<?php echo $id?>">
					<input name="delete" type="submit" id="deleteButton" class="Button" value="-">
				
				</form>
			
				<form class="EventForm" method="POST" action="<?php echo htmlentities($_SERVER["PHP_SELF"]);?>">

					<input name="id" type="hidden" value="<?php echo $id?>">
					Name:
					<br/>
					<input name="name" type="text" size="32" maxlength="32" value="<?php echo $name;?>">
					<br/>
					Date:
					<br/>
					<input name="date" id="dateField" type="text" size="10" maxlength="10" value="<?php echo $date;?>">
					<br/>
					Time:
					<br/>
					<input name="startTime" id="startTimeField" type="text" size="10" maxlength="10" value="<?php echo $startTime;?>">
					to
					<input name="finishTime" id="finishTimeField" type="text" size="10" maxlength="10" value="<?php echo $finishTime;?>">
					<br/>
					HTML:
					<br/>
					<textarea id="htmlField" name="html" rows="20"><?php echo html_entity_decode($html);?></textarea>
					<br/>
					<input name="save" type="submit" id="saveButton" class="Button" value="Save">
				
				</form>
			
			</div>
		
		</div>
		
		<div class="StatusBar">

			<div class="VersionLabel">
			
				<?php echo "Version " . Tetra::$VERSION;?>
			
			</div>
			
			<div class="StatusLabel">
			
				<?php echo $status;?>
		
			</div>
			
		</div>
	
	</body>

</html>