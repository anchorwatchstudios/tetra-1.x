<?php 

require_once "../../src/Tetra.php";
require_once "../../src/XMLCalendarDatabase.php";
require_once "../../src/XMLSettingsDatabase.php";

date_default_timezone_set("UTC");
session_start();
Tetra::loadSkins("../../res/skin/*");
Tetra::loadSettings("../../src/settings.xml");
$skinPath = Tetra::$SETTINGS->getSkin()->getPath();
$calendar = XMLCalendarDatabase::load("../../src/events.xml");
$status = "";

if($_SERVER["REQUEST_METHOD"] == "POST") {
	
	$settings = new Settings();
	$settings->setCalendarName($_POST["name"]);
	$settings->setSkin(Tetra::getSkinByName($_POST["skin"]));
	$settings->setLinksEnabled($_POST["linksEnabled"]);
	$settings->setPopupsEnabled($_POST["popupsEnabled"]);
	$settings->setEventDisplayFormat($_POST["eventDisplayFormat"]);
	$status = XMLSettingsDatabase::save($settings, "../../src/settings.xml");
	header("Location: " . dirname($_SERVER["PHP_SELF"]) . "?status=" . $status);
	exit;
	
}
else if($_SERVER["REQUEST_METHOD"] == "GET") {
	
	if(isset($_GET["status"])) {
	
		$status = Tetra::getStatusById($_GET["status"]);
	
	}
	
}

?>

<!DOCTYPE html>

<html>

	<head>
	
		<link rel="stylesheet" type="text/css" href="<?php echo $skinPath;?>">
	
	</head>
	
	<body>
	
		<div class="Header">
		
			<div class="Logo">TetraCal</div>
			
			<a href="../" id="settingsButton" class="Button">Back</a>
			
			<div class="TitleBar">
			
				<div class="SettingsTitle">Settings</div>
			
			</div>
		
		</div>
		
		<div class="Container">
		
			<form class="SettingsForm" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			
				Calendar Options:
				
				<div class="Tab">
					
					Name:
					<br/>
					<input name="name" type="text" size="32" maxlength="32" value="<?php echo Tetra::$SETTINGS->getCalendarName();?>">
					<br/>
					Event display format:
					<br/>				
					<input value="0" name="eventDisplayFormat" type="radio" <?php if(Tetra::$SETTINGS->getEventDisplayFormat() == Settings::$FORMAT_NO_TIME) echo "checked";?>>
					Name
					<input value="1" name="eventDisplayFormat" type="radio" <?php if(Tetra::$SETTINGS->getEventDisplayFormat() == Settings::$FORMAT_TIME) echo "checked";?>>
					Name and time
					<input value="2" name="eventDisplayFormat" type="radio" <?php if(Tetra::$SETTINGS->getEventDisplayFormat() == Settings::$FORMAT_TIME_RANGE) echo "checked";?>>
					Name and time range
					<br/>
					Enable links
					<input name="linksEnabled" value="true" type="checkbox" <?php if(Tetra::$SETTINGS->getLinksEnabled()) echo "checked";?>>
					<br/>
					Enable popups
					<input name="popupsEnabled" value="true" type="checkbox" <?php if(Tetra::$SETTINGS->getPopupsEnabled()) echo "checked";?>>

				</div>
				
				<br/>
				
				Skin:
				
				<br/>
				
				<select name="skin">
				 
					<?php 
					
						foreach(Tetra::$SKIN as $skin) {
							
							if(Tetra::$SETTINGS->getSkin() == $skin) {
								
								echo "<option value='" . $skin->getName() . "' selected>" . $skin->getName() . "</option>";
								
							}
							else {
								
								echo "<option value='" . $skin->getName() . "'>" . $skin->getName() . "</option>";
								
							}
							
						}
					
					?>
					
				</select> 
			
				<br/>
				
				<input class="Button" id="saveButton" type="submit" value="Save">
			
			</form>
		
		</div>
		
		<div class="StatusBar">

			<div class="VersionLabel">
			
				<?php echo "Version " . Tetra::$VERSION;?>
			
			</div>
			
			<div class="StatusLabel">
			
				<?php echo $status;?>
		
			</div>

		</div>
	
	</body>

</html>