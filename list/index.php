<?php 

require_once "../src/Tetra.php";
require_once "../src/XMLCalendarDatabase.php";

date_default_timezone_set("UTC");
Tetra::loadSkins("../res/skin/*");
Tetra::loadSettings("../src/settings.xml");
$skinPath = Tetra::$SETTINGS->getSkin()->getPath();
$calendar = XMLCalendarDatabase::load("../src/events.xml");
$month = date('n');
$year = date('Y');

if($_SERVER["REQUEST_METHOD"] == "GET") {
	
	if(isset($_GET["month"])) {
		
		$month = $_GET["month"];
		
	}
	
	if(isset($_GET["year"])) {
	
		$year = $_GET["year"];
	
	}
	
}

$calendar->setMonth($month);
$calendar->setYear($year);
$listContents = "";
$events = $calendar->getEvents();
$evenOdd = "ListEventOdd";

foreach($events as $event) {

	if(date_format($event->getStartTime(), "n") == $month && date_format($event->getStartTime(), "Y") == $year) {
	
		if($event->getStartTime() == $event->getFinishTime()) {
			
			$listContents .= "<div class='ListEvent " . $evenOdd . "'><div class='ListEventName'>" . $event->getName() . "</div><div class='ListEventDate'>" . $event->getDate() . "</div><div class='ListEventTime'>" . date_format($event->getStartTime(), "g:ia") . "</div></div>";
			
		}
		else {

			$listContents .= "<div class='ListEvent " . $evenOdd . "'><div class='ListEventName'>" . $event->getName() . "</div><div class='ListEventDate'>" . $event->getDate() . "</div><div class='ListEventTime'>" . date_format($event->getStartTime(), "g:ia") . " - " . date_format($event->getFinishTime(), "g:ia") . "</div></div>";
			
		}
		
		if($evenOdd == "ListEventEven") {
			
			$evenOdd = "ListEventOdd";
			
		}
		else {
			
			$evenOdd = "ListEventEven";
			
		}

	}
	
}

?>

<!DOCTYPE html>

<html>

	<head>

		<link rel="stylesheet" type="text/css" href="<?php echo $skinPath;?>">

	</head>

	<body>
	
		<div class="ListContainer">
		
			<div class="ListTitle">
			
				Upcoming Events:
			
			</div>
			
			<div class="List">
			
				<?php echo $listContents;?>
			
			</div>
					
		</div>
	
	</body>
	
</html>