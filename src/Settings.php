<?php 

require_once "Tetra.php";

class Settings {
	
	public static $FORMAT_NO_TIME = 0;
	public static $FORMAT_TIME = 1;
	public static $FORMAT_TIME_RANGE = 2;
	private $calendarName;
	private $skin;
	private $popup;
	private $link;
	private $eventDisplayFormat;
	
	public function __construct() {
		
		$this->calendarName = "Events";
		$this->skin = Tetra::getSkinByName("Default");
		$this->popup = true;
		$this->link = true;
		$this->displayTime = true;
		$this->displayFormat = Settings::$FORMAT_TIME;
		
	}
	
	public function getCalendarName() {
		
		return $this->calendarName;
		
	}
	
	public function setCalendarName($calendarName) {
		
		$this->calendarName = $calendarName;
		
	}
	
	public function getSkin() {
		
		return $this->skin;
		
	}
	
	public function setSkin($skin) {
		
		$this->skin = $skin;
		
	}
	
	public function getPopupsEnabled() {
		
		return $this->popup;
		
	}
	
	public function setPopupsEnabled($bool) {
		
		$this->popup = $bool;
		
	}
	
	public function getLinksEnabled() {
	
		return $this->link;
	
	}
	
	public function setLinksEnabled($bool) {
		
		$this->link = $bool;
		
	}
	
	public function getEventDisplayFormat() {
	
		return $this->eventDisplayFormat;
	
	}
	
	public function setEventDisplayFormat($format) {
	
		$this->eventDisplayFormat = $format;
	
	}
	
}

?>