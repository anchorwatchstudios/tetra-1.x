<?php 

class DateTimeEvent implements Event {
	
	private static $COUNT = 0;
	private $id;
	private $name;
	private $startTime;
	private $finishTime;
	private $html;
	
	public function __construct($name, DateTime $startTime, DateTime $finishTime, $html) {
		
		$this->id = self::$COUNT;
		$this->name = $name;
		$this->startTime = $startTime;
		$this->finishTime = $finishTime;
		$this->html = $html;
		self::$COUNT++;
		
	}
	
	public function getId() {
		
		return $this->id;
		
	}
	
	public function getName() {
		
		return $this->name;
		
	}
	
	public function getStartTime() {
		
		return $this->startTime;
		
	}
	
	public function getFinishTime() {
		
		return $this->finishTime;
		
	}
	
	public function getHTML() {
	
		return $this->html;
	
	}
	
	public function getDate() {
		
		return date_format($this->startTime, "m/d/Y");
		
	}
	
	public function getYear() {
		
		return intval(date_format($this->startTime, "Y"));
		
	}
	
	public function getMonth() {
	
		return intval(date_format($this->startTime, "n"));
	
	}
	
	public function getDay() {
	
		return intval(date_format($this->startTime, "j"));
	
	}
			
	public static function getCount() {
		
		return self::$COUNT;
		
	}
		
}

?>