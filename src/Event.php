<?php 

interface Event {
	
	function getId();
	
	function getName();
	
	function getStartTime();
	
	function getFinishTime();
	
	function getHTML();
	
	function getDate();
	
	function getYear();
	
	function getMonth();
	
	function getDay();
	
	static function getCount();
		
}

?>