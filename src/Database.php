<?php

interface Database {
	
	static function load($path);
	
	static function save($object, $path);
	
}

?>