<?php 

require_once "Database.php";
require_once "Skin.php";

class XMLSkinDatabase implements Database {
	
	private function __construct() {
		
	}
	
	public static function load($path) {

		$result = array();
		
		foreach(glob($path) as $folder) {
						
			foreach(glob($folder . "/*") as $file) {
				
				if(is_file($file) && preg_match("/\.xml/", $file)) {
					
					$result[] = XMLSkinDatabase::fileToSkin($file);
					
				}
				
			}
		
		}
		
		return $result;
			
	}
	
	public static function save($object, $path) {
		
	}
	
	private static function fileToSkin($file) {
		
		$database = DOMDocument::load($file);
		$xml = $database->documentElement;
		$name = $xml->getAttribute("name");
		$description = "None";
		$path = preg_filter("/\.xml/", ".css", $file);
		
		foreach($xml->childNodes as $child) {
			
			switch($child->tagName) {
				
				case "Description":
					
					$description = XMLSkinDatabase::getInnerXML($database, $child);
					break;
					
				default:
					
					break;
				
			}
			
		}
		
		return new Skin($name, $description, $path);
		
	}
	
	private static function getInnerXML($database, $xml) {
	
		$content="";
	
		foreach($xml->childNodes as $child) {
				
			$content .= $database->saveXML($child);
	
		}
	
		return $content;
	
	}
	
}

?>