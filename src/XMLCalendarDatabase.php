<?php 

require_once "Database.php";
require_once "DateTimeEvent.php";
require_once "ArrayEventCalendar.php";

class XMLCalendarDatabase implements Database {
	
	private function __construct() {
		
	}
	
	public static function load($path) {
		
		$month = date('n');
		$year = date('Y');
		$calendar = new ArrayEventCalendar("Events", $month, $year);
		$database = DOMDocument::load($path);
		$elements = $database->getElementsByTagName("Event");
		
		foreach($elements as $element) {
			
			$name = $element->getAttribute("name");
			$startTime = new DateTime($element->getAttribute("startTime"));
			$finishTime = new DateTime($element->getAttribute("finishTime"));
			$html = XMLCalendarDatabase::getInnerXML($database, $element);
			$calendar->addEvent(new DateTimeEvent($name, $startTime, $finishTime, $html));
			
		}

		return $calendar;
		
	}
	
	public static function save($calendar, $path) {

		$database = new DOMDocument();
		$database->appendChild($database->createElement("Database"));
		
		for($i = 0;$i <= $calendar->getSize();$i++) {
			
			$event = $calendar->getEvent($i);
			
			if(isset($event)) {

				$element = $database->createElement("Event");
				$element->setAttribute("name", $event->getName());
				$element->setAttribute("startTime", $event->getStartTime()->format("Y-m-d H:i:s"));
				$element->setAttribute("finishTime", $event->getFinishTime()->format("Y-m-d H:i:s"));
				$element->appendChild($database->createTextNode(html_entity_decode($event->getHTML())));
				$database->documentElement->appendChild($element);
				
			}
			
		}
		
		$database->save($path);
		
	}
	
	private static function getInnerXML($database, $xml) {
		
		$content="";
		
		foreach($xml->childNodes as $child) {
			
			$content .= $database->saveXML($child);

		}
		
		return $content;
	
	}
	
}

?>