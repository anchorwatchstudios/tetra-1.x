<?php 

require_once "XMLSkinDatabase.php";
require_once "XMLSettingsDatabase.php";

class Tetra {
	
	//TODO: doesn't need to be public here?
	public static $VERSION = "1.0.0";
	public static $DELETED = 0;
	public static $SAVED = 1;
	public static $NAME_ERROR = -1;
	public static $DATE_ERROR = -2;
	public static $TIME_ERROR = -3;
	public static $SAVE_ERROR = -4;
	public static $SETTINGS;
	public static $SKIN;
	
	private function __construct() {
		
	}

	public static function validateInput($values) {
		
		$result = Tetra::$SAVED;
		$dateArray = explode("/", $values["date"]);
		
		if($_POST["name"] == "") {
			
			$result = Tetra::$NAME_ERROR;
			
		}
		else if(sizeof($dateArray) != 3 || !checkdate($dateArray[0], $dateArray[1], $dateArray[2])) {
		
			$result = Tetra::$DATE_ERROR;
		
		}
		else if(!preg_match("/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9](am|AM|pm|PM)$/", $values["startTime"])) {
			
			$result = Tetra::$TIME_ERROR;
			
		}
		else if(!preg_match("/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9](am|AM|pm|PM)$/", $values["finishTime"])) {
			
			$result = Tetra::$TIME_ERROR;
			
		}
		
		return $result;
		
	}
	
	public static function getStatusById($id) {
		
		$result = "";
		
		switch($id) {
			
			case Tetra::$DELETED:
				
				$result = "Delete successful.";
				break;
				
			case Tetra::$SAVED:
				
				$result = "Save successful.";
				break;
				
			case Tetra::$NAME_ERROR:
				
				$result = "Please specify a name.";
				break;
				
			case Tetra::$DATE_ERROR:
				
				$result = "Please enter a date in mm/dd/yyyy format.";
				break;
				
			case Tetra::$TIME_ERROR:
				
				$result = "Please enter a time in hh:mm(am|pm) format.";
				break;
				
			case Tetra::$SAVE_ERROR:
				
				$result = "An error occurred. Failed to save settings.";
				
			default:
				
				$result = "An unexpected error occurred.";
				break;
				
		}
		
		return $result;
		
	}
	
	public static function getSkinByName($name) {
	
		$result = null;
		
		foreach(Tetra::$SKIN as $skin) {

			if($skin->getName() == $name) {
				
				$result = $skin;
				
			}
			
		}
		
		return $result;
	
	}
	
	public static function loadSkins($path) {
		
		Tetra::$SKIN = XMLSkinDatabase::load($path);

	}
	
	public static function loadSettings($path) {
		
		Tetra::$SETTINGS = XMLSettingsDatabase::load($path);
		
	}
			
}

?>