<?php 

require_once "EventCalendar.php";
require_once "Event.php";

class ArrayEventCalendar implements EventCalendar {
	
	private $event;
	private $month;
	private $year;
	
	private static function getDay(DateTime $dateTime) {

		return strtoupper(date_format($dateTime, "D"));
	
	}
	
	public function __construct($month, $year) {
		
		$this->event = array();
		$this->month = $month;
		$this->year = $year;
		
	}
	
	public function addEvent(Event $event) {
		
		$this->event[] = $event;
		
	}
	
	public function removeEvent($id) {
		
		$result = false;
		
		for($i = 0;$i < sizeof($this->event);$i++) {
			
			if($this->event[$i]->getId() == $id) {
				
				unset($this->event[$i]);
				$this->event = array_values($this->event);
				$result = true;
				break;
				
			}
			
		}
		
		return $result;
		
	}
	
	public function getEvent($id) {
		
		$result = null;
		
		foreach($this->event as $e) {
			
			if($e->getId() == $id) {
				
				$result = $e;
				break;
				
			}
			
		}
		
		return $result;
		
	}
	
	public function getEvents() {
		
		return $this->event;
		
	}
	
	public function getSize() {
	
		return sizeof($this->event);
	
	}
	
	public function getYear() {
		
		return $this->year;
		
	}
	
	public function setYear($year) {
		
		$this->year = $year;
		
	}
	
	public function getMonth() {
		
		return $this->month;
		
	}
	
	public function setMonth($month) {
		
		$this->month = $month;	
		
	}
		
	public function getPreviousMonth() {
		
		$result;
		
		if($this->month == 1) {
			
			$result = 12;
			
		}
		else {
			
			$result = $this->month - 1;
			
		}
		
		return $result;
		
	}
	
	public function getNextMonth() {
		
		$result;
		
		if($this->month == 12) {
				
			$result = 1;
				
		}
		else {
				
			$result = $this->month + 1;
				
		}
		
		return $result;
		
	}
	
	public function getPreviousMonthYear() {
	
		$result;
	
		if($this->month == 1) {
				
			$result = $this->year - 1;
				
		}
		else {
				
			$result = $this->year;
				
		}
	
		return $result;
	
	}
	
	public function getNextMonthYear() {
	
		$result;
	
		if($this->month == 12) {
				
			$result = $this->year + 1;
				
		}
		else {
				
			$result = $this->year;
				
		}
	
		return $result;
	
	}
		
	public function toHTML() {
		
		$sortedEvents = $this->sortEvents($this->event);
		$size = $this->getNumberOfDays();
		$firstIndex;

		switch(ArrayEventCalendar::getDay(new DateTime($this->month . "/1/" . $this->year))) {
		
			case "SUN":
		
				$firstIndex = 0;
				break;
		
			case "MON":
		
				$firstIndex = 1;
				break;
		
			case "TUE":
		
				$firstIndex = 2;
				break;
		
			case "WED":
		
				$firstIndex = 3;
				break;
		
			case "THU":
		
				$firstIndex = 4;
				break;
		
			case "FRI":
		
				$firstIndex = 5;
				break;
		
			case "SAT":
		
				$firstIndex = 6;
				break;
		
		}

		$result = "<div class='CalendarTitleTable'><div class='CalendarTitleRow'>";
		$dayArray = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
		
		for($i = 0;$i < 7;$i++) {
				
			$result .= "<div class='CalendarTitleCell'>" . $dayArray[$i] . "</div>";
				
		}
		
		$result .= "</div></div><div class='CalendarContainer'><div class='Calendar'>";
		
		$day = 1;
		$dayIndex = 0;
		$week = 1;
		
		while($day <= $size) {

			$result .= "<div class='CalendarRow CalendarWeek" . strval($week + 1) . "'>";
			$week++;
			
			for($j = 0;$j < 7;$j++) {
				
				$dayClass;
				
				switch($j) {
					
					case 0:
						
						$dayClass = "SundayCell";
						break;

					case 1:
					
						$dayClass = "MondayCell";
						break;
						
					case 2:
						
						$dayClass = "TuesdayCell";
						break;
						
					case 3:
						
						$dayClass = "WednesdayCell";
						break;
						
					case 4:
						
						$dayClass = "ThursdayCell";
						
						break;
						
					case 5:
						
						$dayClass = "FridayCell";
						break;
						
					case 6:
						
						$dayClass = "SaturdayCell";
						break;
					
				}
				
				if($dayIndex >= $firstIndex && $day <= $size) {
					
					$eventText = "";
					
					for($k = 0;$k < sizeof($sortedEvents);$k++) {

						if($sortedEvents[$k]->getDay() == $day) {

							$e = "";
							
							switch(Tetra::$SETTINGS->getEventDisplayFormat()) {
								
								case Settings::$FORMAT_NO_TIME:
									
									$e = $sortedEvents[$k]->getName() . "</br>";
									break;
									
								case Settings::$FORMAT_TIME:
									
									$e = date_format($sortedEvents[$k]->getStartTime(), "g:ia") . " " . $sortedEvents[$k]->getName() . "<br/>";
									break;
									
								case Settings::$FORMAT_TIME_RANGE:
									
									$e = $sortedEvents[$k]->getName() . "<br/>" . date_format($sortedEvents[$k]->getStartTime(), "g:ia") . " - " . date_format($sortedEvents[$k]->getFinishTime(), "g:ia") . "<br/>";
									break;
									
								default:
									
									break;
								
							}
							
							if(Tetra::$SETTINGS->getLinksEnabled()) {
								
								$eventText .= "<div class='CalendarEvent'><a href='" . rtrim(dirname($_SERVER["PHP_SELF"]), "\\") . "/event/?id=" . $sortedEvents[$k]->getId() . "'>" . $e . "</a>";
								
							}
							else {
								
								$eventText .= "<div class='CalendarEvent'>" . $e;
								
							}
							
							if(Tetra::$SETTINGS->getPopupsEnabled() && $sortedEvents[$k]->getHTML() != "") {
								
								$title = "";
								$event = $this->getEvent($sortedEvents[$k]->getId());
									
								if($event->getStartTime() == $event->getFinishTime()) {
								
									$title = date_format($event->getStartTime(), "g:ia");
										
								}
								else {
								
									$title = date_format($event->getStartTime(), "g:ia") . " - " . date_format($event->getFinishTime(), "g:ia");
								
								}
								
								$eventText .= "<div id='popup" . $event->getId() . "' class='Popup'>";
								$eventText .= 
									'<div class="PopupHeader">
			
										<div class="PopupEventTitle">
										
											' . $this->getEvent($event->getId())->getName() . '
											
										</div>
										
										<div class="PopupTitleBar">
										
											<div class="PopupTitle">
											
												' . $title . '
											
											</div>
												
										</div>
									
									</div>';
								$eventText .= html_entity_decode($sortedEvents[$k]->getHTML()) . "</div>";
								
							}
							
							$eventText .= "</div>";
							unset($sortedEvents[$k]);
							$sortedEvents = array_values($sortedEvents);
							$k = -1;
							
						}
						else {
							
							break;
					
						}
							
					}
					
					$result .= "<div class='CalendarCell " . $dayClass . "'><div class='DayNumber'>" . $day . "</div>" . $eventText . "</div>";
					$day++;
					
				}
				else {
					
					$result .= "<div class='CalendarCell EmptyCell " . $dayClass . "'></div>";
					
				}
				
				$dayIndex++;
				
			}
			
			$result .= "</div>";
			
		}
		
		$result .= "</div></div>";
		
		return $result;
		
	}
	
	public function toString() {
	
		$result = null;
	
		switch($this->month) {
				
			case 1:
	
				$result = "January";
				break;
	
			case 2:
	
				$result = "February";
				break;
	
			case 3:
	
				$result = "March";
				break;
	
			case 4:
	
				$result = "April";
				break;
	
			case 5:
	
				$result = "May";
				break;
	
			case 6:
	
				$result = "June";
				break;
	
			case 7:
	
				$result = "July";
				break;
	
			case 8:
	
				$result = "August";
				break;
	
			case 9:
	
				$result = "September";
				break;
	
			case 10:
	
				$result = "October";
				break;
	
			case 11:
	
				$result = "November";
				break;
	
			case 12:
	
				$result = "December";
				break;
	
			default:
	
				$result = "Error";
				break;
					
		}
	
		$result .= " " . $this->year;
	
		return $result;
	
	}
	
	private function getNumberOfDays() {
	
		$result;
	
		switch($this->month) {
				
			case 1:
	
				$result = 31;
				break;
	
			case 2:
	
				if($this->year % 4 == 0) {
						
					if($this->year % 100 == 0) {
	
						if($this->year % 400 == 0) {
								
							$result = 29;
							break;
								
						}
	
						$result = 28;
						break;
	
					}
						
					$result = 29;
					break;
						
				}
				else {
						
					$result = 28;
						
				}
	
				break;
	
			case 3:
	
				$result = 31;
				break;
	
			case 4:
	
				$result = 30;
				break;
	
			case 5:
	
				$result = 31;
				break;
	
			case 6:
	
				$result = 30;
				break;
	
			case 7:
	
				$result = 31;
				break;
	
			case 8:
	
				$result = 31;
				break;
	
			case 9:
	
				$result = 30;
				break;
	
			case 10:
	
				$result = 31;
				break;
	
			case 11:
	
				$result = 30;
				break;
	
			case 12:
	
				$result = 31;
				break;
	
			default:
	
				$result = 0;
				break;
					
		}
	
		return $result;
	
	}
	
	private function sortEvents($events) {
		
		//TODO: Optimize Sorting
		$result = array();
		$unsortedTimes = array();

		while(sizeof($events) > 0) {
			
			$smallest = 999;
			$subResult = null;
			$subIndex = 0;
			
			for($i = 0;$i < sizeof($events);$i++) {

				$year = intval($events[$i]->getYear());
				$month = intval($events[$i]->getMonth());
				$day = intval($events[$i]->getDay());
				
				if($year != $this->year || $month != $this->month) {
					
					unset($events[$i]);
					$events = array_values($events);
					$subResult = null;
					break;
					
				}
				
				if($day < $smallest) {
					
					$subResult = $events[$i];
					$subIndex = $i;
					$smallest = intval($day);
					break;
					
				}
				
			}
			
			if($subResult != null) {

				$unsortedTimes[] = $subResult;
				unset($events[$subIndex]);
				$events = array_values($events);
				
			}
			
		}

		while(sizeof($unsortedTimes) > 0) {
			
			$index = 0;
			$earliest = $unsortedTimes[$index];
			
			for($j = 1;$j < sizeof($unsortedTimes);$j++) {
				
				if($unsortedTimes[$j]->getStartTime() < $earliest->getStartTime()) {
					
					$earliest = $unsortedTimes[$j];
					$index = $j;
					
				}
				
			}
			
			$result[] = $earliest;
			unset($unsortedTimes[$index]);
			$unsortedTimes = array_values($unsortedTimes);
			
		}
		
		return $result;
		
	}
		
}

?>