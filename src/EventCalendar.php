<?php 

require_once "Event.php";

interface EventCalendar {
	
	function addEvent(Event $event);
	
	function removeEvent($id);
	
	function getEvent($id);
	
	function getEvents();
	
	function getSize();
	
	function getYear();
	
	function setYear($year);
	
	function getMonth();
	
	function setMonth($month);
	
	function getPreviousMonth();
	
	function getNextMonth();
	
	function getPreviousMonthYear();
	
	function getNextMonthYear();
	
	function toHTML();
	
	function toString();
	
}

?>