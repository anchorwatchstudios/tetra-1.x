<?php 

require_once "Database.php";
require_once "Settings.php";

class XMLSettingsDatabase implements Database {
	
	private function __construct() {
		
	}
	
	public static function load($path) {
		
		$skin = null;
		$result = new Settings();
		$database = DOMDocument::load($path)->documentElement;
		
		foreach($database->childNodes as $child) {
			
			switch($child->tagName) {
				
				case "Calendar":
					
					$result->setCalendarName($child->getAttribute("name"));
					$result->setEventDisplayFormat($child->getAttribute("eventDisplayFormat"));
					$result->setLinksEnabled($child->getAttribute("link"));
					$result->setPopupsEnabled($child->getAttribute("popup"));
					break;
					
				case "Skin":
					
					$s = Tetra::getSkinByName($child->getAttribute("name"));
					
					if($s != null) {
						
						$result->setSkin($s);
						
					}
					else {
						
						$result->setSkin(Tetra::getSkinByName("Default"));
						
					}

					break;
					
				default:

					break;
				
			}
			
		}
		
		return $result;
		
	}
	
	public static function save($settings, $path) {
		
		$xml = new DOMDocument();
		$rootElement = $xml->createElement("Settings");
		$calendarElement = $xml->createElement("Calendar");
		$calendarNameAttribute = $xml->createAttribute("name");
		$calendarNameAttribute->value = $settings->getCalendarName();
		$calendarElement->appendChild($calendarNameAttribute);
		$eventDisplayFormatAttribute = $xml->createAttribute("eventDisplayFormat");
		$eventDisplayFormatAttribute->value = $settings->getEventDisplayFormat();
		$calendarElement->appendChild($eventDisplayFormatAttribute);
		$linkAttribute = $xml->createAttribute("link");
		$linkAttribute->value = $settings->getLinksEnabled();
		$calendarElement->appendChild($linkAttribute);
		$popupAttribute = $xml->createAttribute("popup");
		$popupAttribute->value = $settings->getPopupsEnabled();
		$calendarElement->appendChild($popupAttribute);
		$rootElement->appendChild($calendarElement);
		$skinElement = $xml->createElement("Skin");
		$skinName = $xml->createAttribute("name");
		$skinName->value = $settings->getSkin()->getName();
		$skinElement->appendChild($skinName);
		$rootElement->appendChild($skinElement);
		$xml->appendChild($rootElement);
		
		if($xml->save($path) == false) {
			
			return Tetra::$SAVE_ERROR;
			
		}
		else {
			
			return Tetra::$SAVED;
			
		}
		
	}
	
}

?>