<?php 

class Skin {
	
	private $name;
	private $description;
	private $path;
	
	public function __construct($name, $description, $path) {
		
		$this->name = $name;
		$this->description = $description;
		$this->path = $path;
		
	}
	
	public function getName() {
		
		return $this->name;
		
	}
	
	public function getDescription() {
	
		return $this->description;
	
	}
	
	public function getPath() {
		
		return $this->path;
		
	}
	
}

?>