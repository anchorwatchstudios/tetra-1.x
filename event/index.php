<?php 

require_once "../src/Tetra.php";
require_once "../src/XMLCalendarDatabase.php";

date_default_timezone_set("UTC");
Tetra::loadSkins("../res/skin/*");
Tetra::loadSettings("../src/settings.xml");
$skinPath = Tetra::$SETTINGS->getSkin()->getPath();
$calendar = XMLCalendarDatabase::load("../src/events.xml");
$id = "";
$html = "";
$event = $calendar->getEvent($id);
$title = "";

if($event->getStartTime() == $event->getFinishTime()) {
		
	$title = date_format($event->getStartTime(), "g:ia");

}
else {
		
	$title = date_format($event->getStartTime(), "g:ia") . " - " . date_format($event->getFinishTime(), "g:ia");
		
}

if($_SERVER["REQUEST_METHOD"] == "GET") {
	
	if(isset($_GET["id"])) {
		
		$id = $_GET["id"];
		$html = $calendar->getEvent($id)->getHTML();
		
	}
	
}

?>

<!DOCTYPE html>

<html>

	<head>
	
		<link rel="stylesheet" type="text/css" href="<?php echo $skinPath;?>">
	
	</head>

	<body>
	
		<div class="Header">
			
			<div class="EventTitle">
			
				<?php echo $calendar->getEvent($id)->getName();?>
				
			</div>
			
			<a href="<?php echo $_SERVER["HTTP_REFERER"];?>" id="settingsButton" class="Button">Back</a>
			
			<div class="TitleBar">
			
				<div class="EventTimeTitle">
				
					<?php echo $title;?>
				
				</div>
					
			</div>
		
		</div>
		
		<?php echo html_entity_decode($html);?>
	
	</body>

</html>