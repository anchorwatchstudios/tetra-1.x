<?php 

require_once "src/Tetra.php";
require_once "src/XMLCalendarDatabase.php";

date_default_timezone_set("UTC");
Tetra::loadSkins("res/skin/*");
Tetra::loadSettings("src/settings.xml");
$skinPath = Tetra::$SETTINGS->getSkin()->getPath();
$calendar = XMLCalendarDatabase::load("src/events.xml");
$month = date('n');
$year = date('Y');

if($_SERVER["REQUEST_METHOD"] == "GET") {
	
	if(isset($_GET["month"])) {
		
		$month = $_GET["month"];
		
	}
	
	if(isset($_GET["year"])) {
	
		$year = $_GET["year"];
	
	}
	
}

$calendar->setMonth($month);
$calendar->setYear($year);

?>

<!DOCTYPE html>

<html>

	<head>

		<link rel="stylesheet" type="text/css" href="<?php echo $skinPath;?>">

	</head>

	<body>
	
		<div class="CalendarHeader">
		
			<a href="?month=<?php echo $calendar->getPreviousMonth() . '&year=' . $calendar->getPreviousMonthYear()?>" id="previousButton" class="CalendarButton">-</a>
			
			<div class="CalendarTitlePanel">
			
				<div class="CalendarTitle"><?php echo $calendar->toString()?></div>
				<div class="CalendarName"><?php echo Tetra::$SETTINGS->getCalendarName()?></div>
			
			</div>
	
			<a href="?month=<?php echo $calendar->getNextMonth() . '&year=' . $calendar->getNextMonthYear()?>" id="nextButton" class="CalendarButton">+</a>
		
		</div>
				
		<?php echo $calendar->toHTML();?>
				
	</body>

</html>